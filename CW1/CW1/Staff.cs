﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW1
{

    /*
     * Jakub Sobczak
     * Stores Veriables
     * 24/10/14
     * */
    public class Staff
    {
        //The following meathod are used to allow other classes to access the private values saved in this class
        private int staffID;
        public int StaffID
        {
            get
            {
                return (staffID);
            }
            set
            {
                staffID = value;
            }
        }
        private string firstName;
        public string FirstName
        {
            get
            {
                return (firstName);
            }
            set
            {
                firstName = value;
            }
        }

        private string secondName;
        public string SecondName
        {
            get
            {
                return (secondName);
            }
            set
            {
                secondName = value;
            }
        }
        private string dateOfBirth;
        public string DateOfBirth
        {
            get
            {
                return (dateOfBirth);
            }
            set
            {
                dateOfBirth = value;
            }
        }
        private string department;
        public string Department
        {
            get
            {
                return (department);
            }
            set
            {
                department = value;
            }
        }

        private int payRate;
        public int PayRate
        {
            get
            {
                return (payRate);
            }
            set
            {
                payRate = value;
            }
        }

        private double hoursWorked;
        public double HoursWorked
        {
            get
            {
                return (hoursWorked);
            }
            set
            {
                hoursWorked = value;
            }
        }
        //This meathos is used to calculate the pay from the amount of hours staff worked
        public double calcPay(int hours)
        {
            double payment;
            payment = hours * PayRate;
            return payment;
        }
        //this meathod is used to establish what tax rate bracket the staff member fits into 10% or 20%
        public int calcTaxRate(int grossPayPence)
        {
            if (grossPayPence < 100000)
            {
                return 10;
            }
            else return 20;
        }
    }
}
