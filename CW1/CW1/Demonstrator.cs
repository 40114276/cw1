﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW1
{
    /*
 * Jakub Sobczak
 * Demonstrator stores extra values for demonstrators but demonstrators are still staff so it inherits the veriables from students class
 * 24/10/14
 * */

    class Demonstrator:Staff
    {
        //the course varible stores details about the course the demontrator is taking
        private String course;
        //this method allows other classes to access the course veriable
        public string Course
        {
            get {
                return (course);
            }
            set
            {
                course = value;
            }
        }
        //The supervisor variable stores the supervisor of the demonstrator
        private String supervisor;
        //this method allows other classes to access the supervisor veriable
        public string Supervisor
        {
            get
            {
                return (supervisor);
            }
            set
            {
                supervisor = value;
            }
        }
        //The check variable allows other meathods and if statements to check if a demonstrator or a staff member is entering the values
        private int demonstratorCheck;
        //this method allows other classes to access the check variable
        public int DemonstratorCheck
        {
            get
            {
                return(demonstratorCheck);
            }
            set
            {
                demonstratorCheck = value;
            }
        }
    }
}
