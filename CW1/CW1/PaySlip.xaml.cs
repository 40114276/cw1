﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CW1
{
    public partial class PaySlip : Window
    {

/* Jakub Sobczak
* Pay Slip class uses previously set values to show a pay slip using a new window
* 24/10/14
* */

        public PaySlip(Staff staffVeriables)
        {
             InitializeComponent();

             Demonstrator demonstrator = new Demonstrator();
             //used to calculate the gross pay
             int grossPay = (Convert.ToInt32(staffVeriables.HoursWorked) * staffVeriables.PayRate)/100;
            
                 lbl_secondName.Content = staffVeriables.SecondName + ", " + staffVeriables.FirstName + " (" + staffVeriables.Department + ")";
                 lbl_hoursWorked.Content = staffVeriables.HoursWorked;
                 lbl_grossPay.Content = "£" + grossPay;
                 lbl_taxRate.Content = staffVeriables.calcTaxRate(grossPay) + "%";
                 lbl_netPay.Content = "£" + ((grossPay * (100 - staffVeriables.calcTaxRate(grossPay)))/100);
                 lbl_course.Content = "";
                 lbl_supervisor.Content = "";
        }

        private void btn_PaySlipClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
           
