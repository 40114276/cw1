﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CW1
{
    public partial class GUI : Window
    {
 /* Jakub Sobczak
 * GUI class is for the main window storing all the code behind the buttons and text boxes.
 * 24/10/14
 * */
        public GUI()
        {
            InitializeComponent();
        }

        Staff staff = new Staff();

        Demonstrator demonstrator = new Demonstrator();
        private void btn_Set_Click(object sender, RoutedEventArgs e)
        {//The followin code catches exceptions and makes sure all validations are met on each veriable
            try
            {
                staff.FirstName = txt_FirstName.Text;
                if (staff.FirstName != "") { }
                else {
                    MessageBox.Show("Please enter your First Name int the text box provided.");
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Please enter your first name into the text box using letters.");
            }
            try
            {
                staff.SecondName = txt_SecondName.Text;
                if (staff.SecondName != "") { }
                else {
                    MessageBox.Show("Please enter Second Name");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter Second Name in the text box");
            }
            try
            {
                staff.DateOfBirth = txt_DateOfBirth.Text;
                if (staff.DateOfBirth != "") { }
                else 
                {
                    MessageBox.Show("Please enter a Date of birth ");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter Second Name in the text box");
            }
            try
            {
                staff.Department = txt_Department.Text;
                if (staff.Department == "")
                {
                    MessageBox.Show("Please enter a department name.");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter a department name");
            }
            try
            {
                staff.StaffID = Convert.ToInt32(txt_Staff.Text);
                if(staff.StaffID <1000 || staff.StaffID > 2000){
                    MessageBox.Show("Please enter a number between 1000 and 2000");
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Please enter a staff id using integers");
            }
            try
            {
                //payRate2 is a variable I used to take in the value in punds and then change it to pence before it gets stored in the staff class
                double payRate2; 
                payRate2 = Convert.ToDouble(txt_PayRate.Text);
                staff.PayRate = Convert.ToInt32(payRate2 * 100);
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter a pay rate");
            }
            try
            {
                staff.HoursWorked = Convert.ToDouble(txt_Hours.Text);
                if (staff.HoursWorked < 1 || staff.HoursWorked > 65)
                {
                    MessageBox.Show("Please enter a number between 1 and 65 for hours worked.");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter the number of hours worked");
            }

            //The following if function checkes if the user is a demonstrator or not and the stores a value of 1 and 0 so other pieces of code can access that information 
            if (txt_Check.Text == "YES" || txt_Check.Text == "yes" || txt_Check.Text == "y" || txt_Check.Text == "Y")
            {
                demonstrator.DemonstratorCheck = 1;
            }
            else if (txt_Check.Text =="")
            {
                demonstrator.DemonstratorCheck = 0;
            }

            if (demonstrator.DemonstratorCheck == 1)
            {
                try
                {
                    demonstrator.Course = txt_Course.Text;
                    demonstrator.Supervisor = txt_Supervisor.Text;

                    if (demonstrator.Course == "" || demonstrator.Supervisor == "")
                    {
                    MessageBox.Show("Please enter the number into the Demonstrator fields");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Please enter the number into the Demonstrator fields");
                }
            }
        }

        private void btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            txt_FirstName.Text = String.Empty;
            txt_SecondName.Text = String.Empty;
            txt_DateOfBirth.Text = String.Empty;
            txt_Department.Text = String.Empty;
            txt_Staff.Text = String.Empty;
            txt_PayRate.Text = String.Empty;
            txt_Hours.Text = String.Empty;
            txt_Course.Text = String.Empty;
            txt_Supervisor.Text = String.Empty;
        }

        private void btn_Get_Click(object sender, RoutedEventArgs e)
        {
            if(demonstrator.DemonstratorCheck == 0){
              double payRate2;
              txt_FirstName.Text = staff.FirstName;
              txt_SecondName.Text = staff.SecondName;
              txt_DateOfBirth.Text = staff.DateOfBirth;
               txt_Department.Text = staff.Department;
               txt_Staff.Text = staff.StaffID.ToString();
               payRate2 = Convert.ToDouble(staff.PayRate) / 100;
              txt_PayRate.Text = payRate2.ToString();
              txt_Hours.Text = staff.HoursWorked.ToString();
            }
            else if (demonstrator.DemonstratorCheck == 1)
            {
                double payRate2;
                txt_FirstName.Text = staff.FirstName;
                txt_SecondName.Text = staff.SecondName;
                txt_DateOfBirth.Text = staff.DateOfBirth;
                txt_Department.Text = staff.Department;
                txt_Staff.Text = staff.StaffID.ToString();
                payRate2 = Convert.ToDouble(staff.PayRate) / 100;
                txt_PayRate.Text = payRate2.ToString();
                txt_Hours.Text = staff.HoursWorked.ToString();
                txt_Course.Text = demonstrator.Course;
                txt_Supervisor.Text = demonstrator.Supervisor;
            }
        }

        private void btn_Calculate_Click_1(object sender, RoutedEventArgs e)
        {
            PaySlip newWin = new PaySlip(staff);

            if (demonstrator.DemonstratorCheck == 1)
            {
                newWin.lbl_course.Content = "Course: " + demonstrator.Course;
                newWin.lbl_supervisor.Content = "Supervisor: " + demonstrator.Supervisor;
            }
         
            newWin.ShowDialog();
        }

    }
}
